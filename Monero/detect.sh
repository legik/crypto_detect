#!/bin/bash
#===============================================================================
#
# FILE:  detect.sh
#
# USAGE:  detect.sh --help
#
# DESCRIPTION:  Script is used to retrieve all useful information about Monero
#               wallets from the Windows OS.
#               It is needed to provide the HDD or/and RAM dump.
#               The results are saved in the ./results directory.
#
# REQUIREMENTS:  volatility, reglookup
#===============================================================================

MEMORY_DUMP="1"
HDD_DUMP="1"
PROFILE="Win7SP1x64" # default OS profile
RESULT_STR="The results are stored in the \"./results\" directory"

timestamp=$(date +"%Y%m%d-%H%M%S")
RESULT_DIR="results/${timestamp}"

#===  FUNCTION  ================================================================
#         NAME:  show_help
#  DESCRIPTION:  Display usage information for this script.
#===============================================================================
function show_help {
    cat  <<- EOT
    Script is used to retrieve all useful information about Monero wallet.
    
    usage : $0 [-m=] [-d=] [-p=] [-h]
    -m  | --memory          specify the memory dump
    -p  | --profile         specify the OS profile
    -d  | --hdd             specify the hdd dump
    -nm | --no-memory       skip memory analysis
    -nd | --no-hdd          skip hdd analysis
    -h  | --help            display this message
EOT
    exit
}

#===  FUNCTION  ================================================================
#         NAME:  strings_dump
#  DESCRIPTION:  Extract strings from the dump and store it into the file.
# PARAMETER  1:  Dump path
# PARAMETER  2:  File name prefix
#===============================================================================
function strings_dump {
    strings $1 > $2_strings.tmp
}

#===  FUNCTION  ================================================================
#         NAME:  keys_search
#  DESCRIPTION:  Search public and private keys in provided file
# PARAMETER  1:  File name prefix
#===============================================================================
function keys_search {
    grep -Eo "[ :\"][A-Za-z0-9]{95}[ :\"]" $1_strings.tmp | \
    sort | uniq > ${RESULT_DIR}/$1/monero-public-keys
    grep -Eo "^(((\b[a-z]+\b)+\s){12})+(\b[a-z]+\b)" $1_strings.tmp | \
    sort | uniq > ${RESULT_DIR}/$1/light-wallet-phrases
    grep -Eo "^(((\b[a-z]+\b)+\s){24})+(\b[a-z]+\b)" $1_strings.tmp | \
    sort | uniq > ${RESULT_DIR}/$1/normal-wallet-phrases
    grep -E "address\"\:\"[a-z0-9A-Z]{95}\"\,\"view_key\"\:\"[a-z0-9]{64}\"" \
    $1_strings.tmp | sort | uniq > ${RESULT_DIR}/$1/light-view-keys
}

#===  FUNCTION  ================================================================
#         NAME:  mem_reg_analysis
#  DESCRIPTION:  Windows registry (in RAM) analysis
#===============================================================================
function mem_reg_analysis {
    volatility -f ${MEMORY_DUMP} --profile=${PROFILE}  \
    printkey -K "Software\monero-project\monero-core" \
    > ${RESULT_DIR}/mem/win-registry-keys
}

#===  FUNCTION  ================================================================
#         NAME:  mem_monero_pid_analysis
#  DESCRIPTION:  Monero process dumping and analysing
#===============================================================================
function mem_monero_pid_analysis {
    PID=$(volatility -f ${MEMORY_DUMP} --profile=${PROFILE} pslist \
    | grep monero-wallet- | awk '{print $3}')
    if [[ $? != 0 ]]; then
        echo "WARN: Monero process finding failed."
    elif [[ ${PID} ]]; then
        volatility -f ${MEMORY_DUMP} --profile=${PROFILE} memdump \
        --dump-dir ${RESULT_DIR}/ -p ${PID}
        strings ${RESULT_DIR}/${PID}.dmp | grep secret_view_key \
        > ${RESULT_DIR}/mem/normal-private-keys-from-process
    else
        echo "WARN: Not found a running Monero wallet. Process analysis is not \
        succeed"
    fi
}

#===  FUNCTION  ================================================================
#         NAME:  market_usage
#  DESCRIPTION:  Search visited crypto currencies markets.
# PARAMETER  1:  File name prefix
#===============================================================================
function market_usage {
    result=""
    grep -Eo '(http|https)://[^/"]+' $1_strings.tmp | sort --unique | \
    awk -F/ '{print $3}' > urls
    python traffic_analyze.py urls > ${RESULT_DIR}/$1/visited-markets
    rm urls
}

#===  FUNCTION  ================================================================
#         NAME:  clean
#  DESCRIPTION:  Deleting tmp files
#===============================================================================
function clean {
   rm *.tmp
}

#===  FUNCTION  ================================================================
#         NAME:  ram_connections
#  DESCRIPTION:  Search for active Monero connections.
#===============================================================================
function ram_connections {
    volatility netscan -f ${MEMORY_DUMP} --profile=${PROFILE} | \
    grep -e ":18081" | sort | uniq > ${RESULT_DIR}/mem/connections
}

#===  FUNCTION  ================================================================
#         NAME:  dump_analysis
#  DESCRIPTION:  Perform the dump analysis
# PARAMETER  1:  Dump path
# PARAMETER  2:  File name prefix
#===============================================================================
function dump_analysis {
    strings_dump $1 $2
    keys_search $2
    market_usage $2
}

#===  FUNCTION  ================================================================
#         NAME:  hdd_key_findings
#  DESCRIPTION:  Monero keys search
#===============================================================================
function hdd_key_findings {
    find ${HDD_DUMP} -iname *.keys > ${RESULT_DIR}/hdd/key-files-location;
}

#===  FUNCTION  ================================================================
#         NAME:  hdd_reg_parsing
#  DESCRIPTION:  Windows registry analysis
# PARAMETER  1:  Dump path
#===============================================================================
function hdd_reg_parsing {
    reglookup -p /Software/monero-project/monero-core $1 \
    >> ${RESULT_DIR}/hdd/win-registry-keys
}

#===  FUNCTION  ================================================================
#         NAME:  hdd_registry_analysis
#  DESCRIPTION:  Windows registry files search
#===============================================================================
function hdd_registry_analysis {
    find ${HDD_DUMP} -iname NTUSER.DAT | \
    while read line; do hdd_reg_parsing "$line"; done
}

#===  FUNCTION  ================================================================
#         NAME:  memory_dump_analysis
#  DESCRIPTION:  Perform memory dump analysis
#===============================================================================
function memory_dump_analysis {
    mkdir -p ${RESULT_DIR}/mem
    dump_analysis ${MEMORY_DUMP} "mem"
    mem_reg_analysis
    mem_monero_pid_analysis
    ram_connections
}

#===  FUNCTION  ================================================================
#         NAME:  hdd_dump_analysis
#  DESCRIPTION:  Perform hdd dump analysis
#===============================================================================
function hdd_dump_analysis {
    mkdir -p ${RESULT_DIR}/hdd
    dump_analysis ${HDD_DUMP}/pagefile.sys "hdd"
    hdd_key_findings
    hdd_registry_analysis
}

# arguments parsing
for i in "$@"
do
case ${i} in
    -m=*|--memory=*)
    MEMORY_DUMP="${i#*=}"
    ;;
    -p=*|--profile=*)
    PROFILE="${i#*=}"
    ;;
    -h|--help)
    show_help
    ;;
    -nm|--no-memory)
    MEMORY_DUMP="0"
    ;;
    -nh|--no-hdd)
    HDD_DUMP="0"
    ;;
    -d=*|--hdd=*)
    HDD_DUMP="${i#*=}"
    ;;
    *)
    echo "WARN: Unknown option"        # unknown option
    show_help
    ;;
esac
done

if [ ${MEMORY_DUMP} == "1" ] && [ ${HDD_DUMP} == "1" ]; then
    show_help
elif [ ${MEMORY_DUMP} == "0" ] && [ ${HDD_DUMP} == "0" ]; then
    show_help
elif [ ${MEMORY_DUMP} == "0" ] && [ ${HDD_DUMP} == "1" ]; then
    show_help
elif [ ${MEMORY_DUMP} == "1" ] && [ ${HDD_DUMP} == "2" ]; then
    show_help
elif [ ${MEMORY_DUMP} == "0" ] && [ ${HDD_DUMP} != "1" ] && \
[ ${HDD_DUMP} != "0" ]; then
    echo "Hard drive analysis started..."
    hdd_dump_analysis
    echo ${RESULT_STR}
elif [ ${HDD_DUMP} == "0" ] && [ ${MEMORY_DUMP} != "1" ] && \
[ ${MEMORY_DUMP} != "0" ]; then
    echo "Memory analysis started..."
    memory_dump_analysis
    echo ${RESULT_STR}
else
    echo "Memory and HDD analysis started..."
    memory_dump_analysis
    hdd_dump_analysis
    echo ${RESULT_STR}
    clean
fi