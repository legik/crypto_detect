from urllib.parse import urlparse
from urllib.request import urlopen
import re

currencies = {'bitcoin', 'zcash', 'monero', 'dash'}
markets = {'http://toption.info/krypto-exchange/',
           'https://mining-cryptocurrency.ru/obmen-kriptovalyuty/',
           'https://mining-cryptocurrency.ru/rejting-luchshie-birzhi-kriptovalyut-2017/'
           }
urls = set()


def parser(url):
    website = urlopen(url)
    html = website.read()
    links = re.findall('"((http|ftp)s?://.*?)"', html.decode("utf-8"))
    for link in links:
        parse_result = urlparse(link[0])
        urls.add(parse_result.netloc)


if __name__ == '__main__':
    for name in currencies:
        parser('https://coinmarketcap.com/currencies/{}/#markets'.format(name))
    for market in markets:
        parser(market)
    print('Parsed urls: {}'.format(len(urls)))
    f = open('urls.txt', 'w')
    for u in urls:
        f.write(str(u) + '\n')
    f.close()
